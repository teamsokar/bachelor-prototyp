﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Globalization;

namespace WpfPrototype6Screens
{
	public class ToleranceConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object paremeter, CultureInfo culture)
		{
			string str = value as string;
			Double val = 0;
			
			if(str.Length > 0)
			{
				switch(str.Substring(0, 1))
				{
					case "+":	str = str.Substring(1);
								if(str == "")
								{
									str = "0";
								}
								val = -(Double.Parse(str));
								break;
					case "-": 	str = str.Substring(1);
								if(str == "")
								{
									str = "0";
								}
								val = Double.Parse(str);
								break;
					case "0":
					case "1":
					case "2":
					case "3":
					case "4":
					case "5":
					case "6":
					case "7":
					case "8":
					case "9": 	val = -(Double.Parse(str));
								break;
					default:
								break;
				}
			}
			
			return val;
		}
		
		public object ConvertBack(object value, Type targetType, object paremter, CultureInfo culture)
		{
			return Binding.DoNothing;
		}
	}
}